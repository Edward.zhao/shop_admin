import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login.vue'
import Home from '@/components/Home.vue'
import Welcome from '@/components/Welcome.vue'

// 路由懒加载模式
// const Login = () => import(/* webpacl: "login_hone_wel" */ '@/components/Login.vue')
// const Home = () => import(/* webpacl: "login_hone_wel" */ '@/components/Home.vue')
// const Welcome = () => import(/* webpacl: "login_hone_wel" */ '@/components/Welcome.vue')

import Users from '@/components/user/Users.vue'
import Rights from '@/components/power/Rights.vue'
import Roles from '@/components/power/Roles.vue'
import Categories from '@/components/goods/Categories.vue'
import Params from '@/components/goods/Params.vue'
import Goods from '@/components/goods/Goods.vue'
import Order from '@/components/order/orders.vue'
import Report from '@/components/report/reports.vue'

Vue.use(Router)

 const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login',
      component: Login
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/home',
      component: Home,
      redirect: '/welcome',
      children:[
        {
        path: '/welcome',
        component: Welcome
        },
        {
          path: '/users',
          component: Users
        },
        {
          path: '/rights',
          component: Rights
        },
        {
          path: '/roles',
          component: Roles
        },
        {
          path: '/categories',
          component: Categories
        },
        {
          path: '/params',
          component: Params
        },
        {
          path: '/goods',
          component: Goods
        },
        {
          path: '/orders',
          component: Order
        },
        {
          path: '/reports',
          component: Report
        }
      ]
    }
  ]
})

// 挂载路由导航守卫
// to 将要访问的路径 
// from 从哪个路径来
// next()放行 
// ntxt("/路径")强制跳转

router.beforeEach((to, from, next) => {

  if(to.path === '/login') return next()

  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  // 如果不存在token强制跳转login页面
  if(!tokenStr) return next('/login')
  // 存在token跳转指定页面
  next()
})



export default router