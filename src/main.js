// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'
import '@/assets/css/global.css'
import { Message } from 'element-ui'
import ZkTable from 'vue-table-with-tree-grid'

// 导入NProgress的js和css包
import NProgress from 'nprogress'
import 'nprogress/nprogress.css' // 美化进度条

Vue.component('tree-table',ZkTable)
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$message = Message
// Vue.prototype$confirm = MessageBox.confirm
// Vue.prototype.$echarts = echarts
// axios请求拦截
// 展示进度条NProgress.start()
axios.interceptors.request.use(config =>{
  NProgress.start()
  // console.log(config);
  // 为请求头对象添加token验证的Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
// 进度条隐藏NProgress.done()
axios.interceptors.response.use(config => {
  NProgress.done()
  return config
})
Vue.prototype.$http= axios

// axios配置请求的根路径
axios.defaults.baseURL = 'http://192.168.2.101:8082/api/private/v1/'

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
